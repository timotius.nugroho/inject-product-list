import { google } from "googleapis";
import axios from "axios";
import https from "https";

const privateKey =
  "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDJOIxhY+PPBluS\nXyR2Btk9shpUNcma95Ggd8+GEVz67QuSmFEFSfcflO24CRHzWoEV5qT2VsBx/uBg\nniVwiLKFAReX+apIM4AZE6/+BtNUsrFQ+qBclxE3FX/fKBSCEnX2bW7o9KW2MfUT\ntQHJf3G6DYw3qzdXUuzeVZ6ykZoQqnLFuFA+RBvv8qZTy4lShOn3ZCWuFopRewrP\n+Yv3pBzdwwselDN50c6SRwr6sCzbFq1kRMqz0YwZAJyPNhSroGwDlhKad9GMeWRn\nq8VHcnnz8+VtZuJZVJiYB4AzNYMgw4rDrq50ctuFuoB6ZdLW0qUX/KM6TeK9lTtr\nzoeHKAv9AgMBAAECggEAEvPTTxH+QT/XTKpO8Bn1iLjXf4KAbmruibd3Mai9gfnl\nOuXDTdT5ZH1dxZV/HhVybppmg7hIkIQh5J/IpXiOnfXsH7mZhwrowBPpYesJKSNa\njabJh2cILVDGVkG7asPr3e3RQjpWyFocvAIyvKSXFN/VSuICyKQMIOXeGHIb2YFV\nhemZ4O5fH8K6CryIWYz+EcEwfhjNQxsFb1PkCtQQ4o9QFbmHJngTFeMlzrZy504D\nO+uB/XsCESbcscfRmD3LvumQfrituk0mzs99v88OGC1b1sF0OMZtJsmW0ocG5cPF\nWCoH35wgh247Lcq2DUXkwoWwqEGrwqVDqoYd6hefgQKBgQD0iK9xItUpoq0Fgb1M\nQbg2wKu8vSgqaLq3rVEGI6H1+gpSuXpyNsqLpBW+wI0Jz3VkVnGOQM7RZ03wI6PS\nyQxMMu7lA8oHHtGUL+ozmGZmZ4chCuuIN+AZB2B7nowlfgRfd5zGJhToc2ZNEREC\nRJ805ADmnQgJ5Y5DiPoFm2dUfQKBgQDSp/Ikj+Nw1ErPYqrggXTDRi3MWXYEQjIn\nrakzvAMTgZl8RLn/9/F5x4/A/75aiR6FSQ8IcW9NdYOGihg4EwXs9gRxhOvgmKWZ\ntCFjmGP4n4kKWB9N+D7zKTsCIQWAEQ6QW7Mutdyh1bIPJIhnG1+y1RCkUvC5TRp1\nNvS2qlatgQKBgGWTTYUqWe9UkpiB/vbpKbMcr3nT/RY4sK7RgKGTNZiuh5E7WICO\n/+2neBzncO6NTY8jVUIo64AScE57D3bkaoZQdemNfGMeyyHKEXfjK04IXRo9pf7M\nbvsWlF5lSoPcZ8acMfQhJ3gzT2Bd4XnWtWdL+o/Q1OD/iOvGv0yK9A4lAoGASEOY\ny3YY3dHVwALm/5EpTvu6JkniNF/I+NeYHck3fO+OiZTCAc86pT4SUHDrCf/T2oFP\niOPw/K99aR/iYwoTlFBpAdBJYoi69llWwuW6FcYUqtnEE2k/nwbb2UUer+oqBteO\nRk3Y3COEq7EqocRANgblaJmTPt32X5N0q10P0wECgYAH1wQ/xbCmH3NM0DwL1q6s\ncZJJ9KCKkbwFDsynTy7UG7C5En/sxK9fEIelAVKaiBu2i8HHCprASD6g//uxSYDn\ndR+ISbtCFs0CBKAwx8gtIpQWe96DA05pVLaxXb20aOcnmBPBD6O3HuYS6jDMKYc9\nW9x4uU6t0xvQ8CCxxbVftg==\n-----END PRIVATE KEY-----\n";
const token =
  "7162ee082b16136a780ff5bfdcde0d2d92d7716f97b6c0b83488b355f25c022a73f2ace66ca003640c52d253b5ece833ddec4e181660a2d7438fd66121720a21974c00c8d392abb17abe42d5d7a423881bc69d12b021dcb395f1826831e3d9f02bf1b9d9b30a04c58f844512d811db2ea819d1be9ed1746d1032119d715a4965";
const baseDomain = "staging-cms.aruna.id";

const getGSheetListProduct = async () => {
  const auth = new google.auth.GoogleAuth({
    credentials: {
      client_email:
        "boat-dash-frontend@api-project-928654461237.iam.gserviceaccount.com",
      client_id: "108119182567041094723",
      private_key: privateKey?.replace(/\\n/g, "\n"),
    },
    scopes: [
      `${"https://www.googleapis.com/auth"}/drive`,
      `${"https://www.googleapis.com/auth"}/drive.file`,
      `${"https://www.googleapis.com/auth"}/spreadsheets`,
    ],
  });

  const sheets = google.sheets({
    auth,
    version: "v4",
  });

  const response = await sheets.spreadsheets.values.get({
    spreadsheetId: "1PovohWxZQ1fIXv0xMq_2B0kS1L8Jb2Ki3HKS4d86qxE",
    range: "Product Website Aruna Seafood", // sheet name
  });

  const rows = response?.data?.values ?? [];
  return rows.slice(1);
};

const getProductCategroyId = async (name) => {
  const res = await axios.get(
    `https://${baseDomain}/api/as-product-categories?filters[name][$eq]=${name}`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
      httpsAgent: new https.Agent({
        rejectUnauthorized: false,
      }),
    }
  );

  let id = res?.data?.data[0]?.id;
  console.log("FOUND_CATEGORY_ID", id);
  if (id) return id;

  const resPost = await axios.post(
    `https://${baseDomain}/api/as-product-categories`,
    {
      data: {
        name: name,
        slug: name
          .toLowerCase()
          .replace(/^\s+|\s+$/g, "")
          .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
          .replace(/\s+/g, "-") // collapse whitespace and replace by -
          .replace(/-+/g, "-"),
      },
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
      httpsAgent: new https.Agent({
        rejectUnauthorized: false,
      }),
    }
  );

  id = resPost?.data?.data?.id;
  console.log("NEW_CATEGORY_ID", id);
  return id;
};

const createProduct = async () => {
  try {
    const gSheetproducts = await getGSheetListProduct();

    for (let e of gSheetproducts) {
      const categoryName = e[0];
      const categoryId = await getProductCategroyId(categoryName);
      await new Promise((r) => setTimeout(r, 500));

      const payload = {
        name: e[1],
        slug: e[1]
          .toLowerCase()
          .replace(/^\s+|\s+$/g, "")
          .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
          .replace(/\s+/g, "-") // collapse whitespace and replace by -
          .replace(/-+/g, "-"),
        description: e[4],
        processing_type: e[5],
        size: e[6],
        packaging: e[7],
        publishedAt: null,
        aruna_seafood_product_category: categoryId,
      };
      console.log(payload);

      await axios.post(
        `https://${baseDomain}/api/as-products`,
        {
          data: payload,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
          httpsAgent: new https.Agent({
            rejectUnauthorized: false,
          }),
        }
      );
    }
  } catch (error) {
    console.log(JSON.stringify(error.response.data.error));
  }
};

createProduct();
// getProductCategroyId("dsdas");
